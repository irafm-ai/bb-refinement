from datetime import datetime
from datetime import timedelta


class Progress:

    def __init__(self, maximum, minimum=0):
        assert maximum > 0, "Maximum {} must be greate than zero.".format(maximum)
        assert minimum < maximum, "Minimum {} must be below maximum {}.".format(minimum, maximum)
        self._start_time = datetime.now()
        self._minimum = minimum
        self._maximum = maximum
        self._current = minimum
        self._current_time = datetime.now()

    def update(self, value):
        assert value >= self._minimum, "Value {} is below minimum {}.".format(value, self._minimum)
        assert value <= self._maximum, "Value {} is above maximum {}.".format(value, self._maximum)
        self._current = value
        self._current_time = datetime.now()

    def get_percentage(self):
        ret = (self._current - self._minimum) / (self._maximum - self._minimum)
        return ret

    def get_percentage_string(self, decimals_count=0):
        pattern = "{:." + str(decimals_count) + "f}%"
        return pattern.format(self.get_percentage() * 100.0)

    def get_seconds_elapsed(self) -> timedelta:
        ret = self._current_time - self._start_time
        return ret

    def get_seconds_total(self) -> timedelta:
        perc = self.get_percentage()
        if perc == 0: return None
        secs = self.get_seconds_elapsed()
        ret = secs / perc
        return ret

    def get_seconds_left(self) -> timedelta:
        total = self.get_seconds_total()
        if total is None:
            return None
        elapsed = self.get_seconds_elapsed()
        ret = total - elapsed
        return ret

    def get_time_total(self) -> datetime:
        left = self.get_seconds_left()
        if left is None:
            return None
        now = datetime.now()
        now = now + left
        return now
