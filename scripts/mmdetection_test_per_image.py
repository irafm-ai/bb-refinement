from mmdet.apis import init_detector, inference_detector
from mmdet.apis import show_result_pyplot
import mmcv
import argparse
import pickle

def parse_args():
  parser = argparse.ArgumentParser(
    description="MMDet per image evaluator written by Marek Vajgl"  )

  parser.add_argument("config", help="test config file (py) path")
  parser.add_argument("checkpoint", help="checkpoint (pth) file")
  parser.add_argument("image", help="tested image path")
  parser.add_argument("out", help="output result file in pickle format")

  args = parser.parse_args()

  return args


def main():
  args = parse_args()

  print("Config: {}".format(args.config))
  print("Model: {}".format(args.checkpoint))
  print("Input: {}".format(args.image))
  print("Output: {}".format(args.out))

  print("\nLoading model...")
  model = init_detector(
    args.config,
    args.checkpoint)

  print("\nInferencing over image...")
  result = inference_detector(model, args.image)

  print("\nSaving result...")
  pickle.dump(result, open(args.out, "wb"))

  print("\nDone")

if __name__ == '__main__':
    main()