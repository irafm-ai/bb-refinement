from mmdet.apis import init_detector, inference_detector
from mmdet.apis import show_result_pyplot
import mmcv
import argparse
import pickle
import os


def parse_args():
    parser = argparse.ArgumentParser(
        description="MMDet per image evaluator written by Marek Vajgl",
        allow_abbrev=False)

    parser.add_argument("config", help="test config file (py) path")
    parser.add_argument("checkpoint", help="checkpoint (pth) file")
    parser.add_argument("input_folder", help="tested images path")
    parser.add_argument("output_folder",
                        help="output folder where will be result files in pickle format")
    parser.add_argument("--minimize", action="store_true", help="when used, only bb-box part of record will be stored")

    args = parser.parse_args()

    return args


def get_all_images(path):
    ret = [os.path.join(path, q) for q in os.listdir(path) if q.endswith(".jpg")]
    return ret


def get_pure_file_name(path):
    ret = os.path.basename(path)
    ret = os.path.splitext(ret)
    ret = ret[0]
    return ret


def main():
    args = parse_args()

    print("Config: {}".format(args.config))
    print("Model: {}".format(args.checkpoint))
    print("Input folder: {}".format(args.input_folder))
    print("Output folder: {}".format(args.output_folder))

    print("\nLoading model...")
    model = init_detector(
        args.config,
        args.checkpoint)

    print("\nLoading images")
    image_files = get_all_images(args.input_folder)
    print("...loaded ", len(image_files), " images")

    print("\nInferencing over images")
    for index, image_file in enumerate(image_files):
        input_file = os.path.basename(image_file)
        output_file = get_pure_file_name(image_file) + ".pck"
        print(index, ". ", input_file, " => ", output_file)
        output_file = os.path.join(args.output_folder, output_file)

        result = inference_detector(model, image_file)

        if args.minimize:
            result = (result[0], [])

        pickle.dump(result, open(output_file, "wb"))



    print("\nDone")


if __name__ == '__main__':
    main()
