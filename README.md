# About this repository
We present a conceptually simple yet powerful and flexible scheme for refining predictions of boxes. Our approach can be built on top of an arbitrary object detector and produces more precise predictions. The method, called BBRefinement, utilizes mixture data of image information together with the object's class and center. Due to transformation of the problem into a domain where BBRefinement does not care about multiscale detection or multiple detections, the training is much more effective, resulting in the ability to refine even ground truth labels into more precise form. In the benchmark, BBRefinement improves the performance of SOTA architectures up to 2mAP points. The process of refinement is fast, able to run in real-time on standard hardware. 
This is a reference implementation of our [ArXiv paper](TODO)

![Scheme of BBRefinement pipeline](pr_materials/BBRefinement.png)



# The training of BBRefinement
Firstly, it is necessary to prepare the data. [The script for training refiner](https://gitlab.com/irafm-ai/bb-refinement/-/blob/master/scripts/train_refiner_iclr.ipynb) accepts inputs in .txt format. We recommend having three txt files for the train, valid, and test. The files use the notation of one image per line. One line includes all the boxes inside an image as follows:
```
path_to\image1.jpg x1,y1,x2,y2,class x1,y1,x2,y2,class
path_to\image2.jpg x1,y1,x2,y2,class
```
Where x1,y1 denote top-left of a bounding box and x2,y2 denote bottom-right. 

Then, when data is prepared, it is enough to run all the cells in our reference training script. The output is .h5 model of BBRefinement, which can be used for improving the mAP of SOTA architectures. The way of integrating with SOTA is shown bellow.


## Pretrained models
We offer five models pre-trained on the COCO dataset. The models differ by the used backbone.
- [BBRefinement with EfficicientNet 0 backbone](https://gitlab.com/irafm-ai/bb-refinement/-/blob/master/models/refiner_b0.zip)
- [BBRefinement with EfficicientNet 1 backbone](https://gitlab.com/irafm-ai/bb-refinement/-/blob/master/models/refiner_b1.zip)
- [BBRefinement with EfficicientNet 2 backbone](https://gitlab.com/irafm-ai/bb-refinement/-/blob/master/models/refiner_b2.zip)
- [BBRefinement with EfficicientNet 3 backbone](https://gitlab.com/irafm-ai/bb-refinement/-/blob/master/models/refiner_b3.zip)
- [BBRefinement with EfficicientNet 4 backbone](https://gitlab.com/irafm-ai/bb-refinement/-/blob/master/models/refiner_b4.zip)

The achieved precision of the five models is shown in the table bellow.
![Accuracies of the models](pr_materials/performance.png)

## Dependencies
- tensorflow 1.15.0
- keras 2.3.0
- opencv 4.1.0,
- numpy 1.17.0



# The integration with SOTA architectures

We have tested our implementation on detectors provided by two frameworks - Detectron2 and MMDetection. All the implementations were built (and are presented) using notebooks executed on `Google Colab`.

The evaluation consists of several steps:
1. Evaluation of the dataset using the arbitrary object detector. For this step, we have used Detectron2 or MMDetection frameworks
2. Refining the box using the BBRefinement. For this step, Keras/Pytorch libraries are required.
3. Evaluation using _pycocotools_. This step requires _pycocotools_ installation.

All the requirements and the whole procedure is described in Jupyter Notebooks. You can find them in the `/Demo` folder in the repository. All the models are available to download either from this GitHub repository, or directly from `wget` - see the examples in the scripts in the `/Demo` folder.

## Detectron2
The implementation of Detectron2 was taken from the [official Detectron2 website](https://github.com/facebookresearch/detectron2). 
The notebook [Detectron2 + Refiner.ipynb](https://gitlab.com/irafm-ai/bb-refinement/-/blob/master/demo/Detectron2_+_Refiner.ipynb) describes all the three steps mentioned above in a single file.

In the file, you have only to choose the appropriate SOTA architecture from the Detectron2 model ZOO and the required BBRefinement model. The notebook then:
1. Initializes the (Colab) environment and installs required packages.
2. Downloads COCO dataset
3. Downloads BBRefinement files
4. Download and install Detectron2 framework and the appropriate model
5. Evaluates COCO dataset using Detectron2 model and BBRefinement model
6. Evaluates mAP before and after refinement.

## MMDetection

The implementation for detectors included in MMDetection framework is based again on the [official MMDetection website](https://github.com/open-mmlab/mmdetection). The procedure here consists of two notebooks:
1. [MMDetection - step 1: Evaluation -> pickle.ipynb](https://gitlab.com/irafm-ai/bb-refinement/-/blob/master/demo/MMDetection_step_1_Evaluation_%20_pickle.ipynb) file describes the step when MMDetection based object detector processes detections. The result is stored in the output pickle file. The notebook:
   1. Initializes the (Colab) environment and installs mmdetection and all the required packages
   2. Downloads COCO dataset
   3. Downloads the mmdetection model selected by the user
   4. Evalutates the whole dataset using the model
   5. Packs the result pickle files in the ZIP file and uploads it on Google Drive (let the file can be processed in step 2).
2. [MMDetection - step 2: pickle -> Refiner -> PyCocoTools.ipynb](https://gitlab.com/irafm-ai/bb-refinement/-/blob/master/demo/MMDetection_step_2_pickle_%20_Refiner_%20_PyCocoTools.ipynb) file describes the step when the pickle file from the previous step is loaded, and data are transferred for the usage in BBRefinement. Then, they are refined and mAP is evaluated using _pycocotools_. The notebook:
   1. Initializes the (Colab) environment and installs required packages.
   2. Downloads COCO dataset
   3. Downloads BBRefinement files
   4. Downloads and unpacks the ZIP file from the step 1 (see above). Also converts the file in the valid format for further processing
   5. Applies the BBRefinement process
   6. Evaluates mAP before and after refinement.

Note that in Step 1, you can choose if you clone mmdetection framework from some git repository, or download our snapshot zipped version from this repository (see the descrption in the notebook file).